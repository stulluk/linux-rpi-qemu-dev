# RPI-QEMU-HELLO-MODULE

These are Kernel module development example source for TAU Lab

# Compiling

- Only run: "make" 

This will create hello.ko file

# Cleaning

- make clean

# Required Commands to run on the qemu rpi

- First, open a terminal. This terminal will be used for running QEMU
- In this terminal, start QEMU-RPI:
- Go to /home/dev/rpi-qemu-dev
- Run :  make run-rpi

And then wait for 1 minute for the qemu-rpi to startup


Now open a new terminal, and split it vertically by pressing CTRL+E and go to this directory:
- cd /home/dev/linux-rpi

- On the RIGHT terminal we need to follow dmesg:

sshpass -p raspberry ssh -p 5022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@127.0.0.1 "dmesg -wT"

You will see that dmesg will be opened and followed until you press CTRL+C

- On the LEFT terminal, run below commands


## Copy generated hello.ko to qemu-rpi
sshpass -p raspberry scp -P 5022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null hello.ko pi@127.0.0.1:/home/pi/

## Insmod hello.ko to qemu-rpi
sshpass -p raspberry ssh -p 5022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@127.0.0.1 "sudo insmod /home/pi/hello.ko"

## Rmmod hello.ko from qemu-rpi
sshpass -p raspberry ssh -p 5022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@127.0.0.1 "sudo rmmod /home/pi/hello.ko 2>/dev/null"
