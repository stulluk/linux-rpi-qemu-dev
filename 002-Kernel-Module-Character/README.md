# RPI-QEMU-EBBCHAR-MODULE

These are Kernel module development example source for TAU Lab

# Compiling

- Only run: "make" 

This will create ebbchar.ko file and test executable

# Cleaning

- make clean

# Required Commands to run on the qemu rpi

- First, open a terminal. This terminal will be used for running QEMU
- In this terminal, start QEMU-RPI:
- Go to /home/dev/rpi-qemu-dev
- Run :  make run-rpi

And then wait for 1 minute for the qemu-rpi to startup


Now open a new terminal, and split it vertically by pressing CTRL+E and go to this directory:
- cd /home/dev/linux-rpi

- On the RIGHT terminal we need to follow dmesg:

sshpass -p raspberry ssh -p 5022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@127.0.0.1 "dmesg -wT"

You will see that dmesg will be opened and followed until you press CTRL+C

- On the LEFT terminal, run below commands


## Copy generated ebbchar.ko and test executable to qemu-rpi
sshpass -p raspberry scp -P 5022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null test ebbchar.ko pi@127.0.0.1:/home/pi/

## Insmod ebbchar.ko to qemu-rpi
sshpass -p raspberry ssh -p 5022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@127.0.0.1 "sudo insmod /home/pi/ebbchar.ko"

## Rmmod ebbchar.ko from qemu-rpi
sshpass -p raspberry ssh -p 5022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@127.0.0.1 "sudo rmmod /home/pi/ebbchar.ko 2>/dev/null"

# Enter shell of qemu-rpi
sshpass -p raspberry ssh -p 5022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@127.0.0.1 

#This will drop you shell of rpi in qemu. Now you can execute test 
sudo ./test


