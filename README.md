# linux-rpi-qemu-dev

These are Kernel module development example source for TAU Lab

#Prequisities

All dependencies are already installed in the VM, such as cross compiler and kernel source code.

For testing, you can run:

- arm-linux-gnueabi-gcc -v
- ls ~/linux-rpi-qemu

# Compiling & running

There are subdirectories in this repo, such as 001-XXX and 002-XXX

Each LAB example has their own procedure. Please refer to README files in corresponding directory.

![](TAU-QEMU-RPI-KERNEL.png)

